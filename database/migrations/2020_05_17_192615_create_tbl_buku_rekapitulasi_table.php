<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBukuRekapitulasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_buku_rekapitulasi', function (Blueprint $table) {
            $table->id();
            $table->string('bukurekap_skpd', 100);
            $table->string('bukurekap_norek', 100);
            $table->string('bukurekap_namarek', 100);
            $table->string('bukurekap_apbd', 100);
            $table->integer('bukurekap_thnanggaran');
            $table->integer('bukurekap_page');
            $table->date('bukurekap_date');
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->string('bukurekap_bendahara', 100);
            $table->string('bukurekap_nip', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_buku_rekapitulasi');
    }
}
