<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblKwitansiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kwitansi', function (Blueprint $table) {
            $table->id();
            $table->string('kwitansi_number', 100);
            $table->string('kwitansi_rek', 100);
            $table->date('kwitansi_date');
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->string('kwitansi_anggaran', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kwitansi');
    }
}
