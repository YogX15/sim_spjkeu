<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDetailBeritaAcaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_berita_acara', function (Blueprint $table) {
            $table->id();
            $table->foreignId('berita_id')->constrained('tbl_berita_acara');
            $table->string('dberita_nama', 100);
            $table->string('dberita_vol', 100);
            $table->string('dberita_sat', 100);
            $table->integer('dberita_phrgsat');
            $table->integer('dberita_pjml');
            $table->integer('dberita_nhrgsat');
            $table->integer('dberita_njml');
            $table->string('dberita_status', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_berita_acara');
    }
}
