<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDftrRekapitulasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_dftr_rekapitulasi', function (Blueprint $table) {
            $table->id();
            $table->string('dafrekap_rekening', 100);
            $table->string('dafrekap_uraian', 100);
            $table->date('dafrekap_date');
            $table->string('dafrekap_penerima', 100);
            $table->string('dafrekap_norek', 100);
            $table->string('dafrekap_namabank', 100);
            $table->integer('dafrekap_jmlkotor');
            $table->integer('dafrekap_ppn');
            $table->integer('dafrekap_pph21');
            $table->integer('dafrekap_pph22');
            $table->integer('dafrekap_pph23');
            $table->integer('dafrekap_pphfinal');
            $table->integer('dafrekap_jmlbersih');
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->string('dafrekap_subbidang', 100);
            $table->string('dafrekap_komitmen', 100);
            $table->string('dafrekap_nip', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_dftr_rekapitulasi');
    }
}
