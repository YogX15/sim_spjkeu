<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblKartuKendaliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kartu_kendali', function (Blueprint $table) {
            $table->id();
            $table->string('kartu_skpd', 100);
            $table->string('kartu_program', 100);
            $table->string('kartu_kegiatan', 100);
            $table->string('kartu_pptk', 100);
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kartu_kendali');
    }
}
