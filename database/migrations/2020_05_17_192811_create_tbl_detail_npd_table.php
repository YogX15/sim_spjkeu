<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDetailNpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_npd', function (Blueprint $table) {
            $table->id();
            $table->foreignId('npd_id')->constrained('tbl_npd');
            $table->string('dnpd_rekening', 100);
            $table->string('dnpd_uraian', 100);
            $table->string('dnpd_anggaran', 100);
            $table->string('dnpd_akutansi', 100);
            $table->string('dnpd_pencairan', 100);
            $table->string('dnpd_sisa', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_npd');
    }
}
