<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblSuratTagihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_surat_tagihan', function (Blueprint $table) {
            $table->id();
            $table->string('surat_nomor', 100);
            $table->date('surat_date');
            $table->string('surat_lampiran', 100);
            $table->string('surat_sifat', 100);
            $table->string('surat_nokeg', 100);
            $table->string('surat_namabid', 100);
            $table->string('surat_uraiankeg', 100);
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_surat_tagihan');
    }
}
