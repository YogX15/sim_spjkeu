<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDetailNotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_nota', function (Blueprint $table) {
            $table->id();
            $table->foreignId('nota_id')->constrained('tbl_nota');
            $table->integer('dnota_banyaknya');
            $table->string('dnota_name', 100);
            $table->integer('dnota_price');
            $table->integer('dnota_jumlah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_nota');
    }
}
