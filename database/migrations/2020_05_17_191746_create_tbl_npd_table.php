<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblNpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_npd', function (Blueprint $table) {
            $table->id();
            $table->string('npd_nomor', 100);
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->string('npd_program', 100);
            $table->string('npd_kegiatan', 100);
            $table->string('npd_nodpa', 100);
            $table->string('npd_thnanggaran', 100);
            $table->string('npd_dana', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_npd');
    }
}
