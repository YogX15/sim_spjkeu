<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblPesananBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_pesanan_barang', function (Blueprint $table) {
            $table->id();
            $table->string('pesanan_number', 100);
            $table->string('pesanan_perusahaan', 100);
            $table->string('pesanan_alamat', 100);
            $table->string('pesanan_kegiatan', 100);
            $table->string('pesanan_cetak', 100);
            $table->date('pesanan_date');
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_pesanan_barang');
    }
}
