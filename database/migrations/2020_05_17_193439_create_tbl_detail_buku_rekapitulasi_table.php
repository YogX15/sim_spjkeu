<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDetailBukuRekapitulasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_buku_rekapitulasi', function (Blueprint $table) {
            $table->id();
            $table->string('dbukurekap_bku', 100);
            $table->string('dbukurekap_ls', 100);
            $table->string('dbukurekap_tu', 100);
            $table->string('dbukurekap_jml', 100);
            $table->foreignId('bukurekap_id')->constrained('tbl_buku_rekapitulasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_buku_rekapitulasi');
    }
}
