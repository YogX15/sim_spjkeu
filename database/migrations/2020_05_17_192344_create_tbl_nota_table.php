<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblNotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_nota', function (Blueprint $table) {
            $table->id();
            $table->date('nota_date');
            $table->string('nota_tuan', 100);
            $table->string('nota_toko', 100);
            $table->string('nota_number', 100);
            $table->string('nota_komitmen', 100);
            $table->string('nota_nip', 100);
            $table->string('nota_terima', 100);
            $table->string('nota_kami', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_nota');
    }
}
