<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBeritaAcaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_berita_acara', function (Blueprint $table) {
            $table->id();
            $table->string('berita_nomor', 100);
            $table->date('berita_date');
            $table->string('berita_thnanggaran', 100);
            $table->string('berita_nama', 100);
            $table->foreignId('pejabat_id')->constrained('master_pejabat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_berita_acara');
    }
}
