<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDetailKartuKendaliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_detail_kartu_kendali', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kartu_id')->constrained('tbl_kartu_kendali');
            $table->string('detailk_rekening', 100);
            $table->string('detailk_pagutu', 100);
            $table->string('detailk_paguls', 100);
            $table->string('detailk_uraian', 100);
            $table->string('detailk_realtu', 100);
            $table->string('detailk_realls', 100);
            $table->string('detailk_sisa', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_detail_kartu_kendali');
    }
}
