<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterPejabatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_pejabat', function (Blueprint $table) {
            $table->id();
            $table->string('pejabat_fullname', 100);
            $table->string('pejabat_nip', 100);
            $table->string('pejabat_position', 100);
            $table->string('pejabat_phone', 100);
            $table->string('pejabat_alamat', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_pejabat');
    }
}
