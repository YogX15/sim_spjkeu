<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
               'name'=>'Admin',
               'email'=>'admin@gmail.com',
               'level'=>'1',
               'password'=> Hash::make('123456'),
            ],
            [
               'name'=>'User',
               'email'=>'user@gmail.com',
               'level'=>'0',
               'password'=> Hash::make('123456'),
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
