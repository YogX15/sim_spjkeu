<?php

namespace App\Http\Controllers\Admin;

use App\MasterRekaman;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use App\Http\Controllers\Controller;

class MasterRekamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data_rekaman = MasterRekaman::all();
        return view('admin.data_rekaman', ['data_rekaman' => $data_rekaman]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Alert::success('Success', 'Data Tersimpan');
        MasterRekaman::create($request->all());
        return redirect('/admin/data_rekaman');
        // return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterRekaman  $masterRekaman
     * @return \Illuminate\Http\Response
     */
    public function show(MasterRekaman $masterRekaman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterRekaman  $masterRekaman
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterRekaman $masterRekaman)
    {
        //
        $output = '
            <div class="form-group">
                <label for="e_fullname">Fullname</label>
                <input type="hidden" name="e_id" class="form-control" id="e_id" value="'.$masterRekaman->id.'">
                <input type="text" name="e_rekaman_fullname" class="form-control" id="e_fullname" value="'.$masterRekaman->rekaman_fullname.'">
            </div>
            <div class="form-group">
                <label for="e_alamat">Alamat</label>
                <input type="text" name="e_rekaman_alamat" class="form-control" id="e_alamat" value="'.$masterRekaman->rekaman_alamat.'">
            </div>
            <div class="form-group">
                <label for="e_phone">Phone</label>
                <input type="text" name="e_rekaman_phone" class="form-control" id="e_phone" value="'.$masterRekaman->rekaman_phone.'">
            </div>
        ';
        echo $output;
        // return $masterRekaman;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterRekaman  $masterRekaman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterRekaman $masterRekaman)
    {
        //
        Alert::success('Success', 'Data Terupdate');
        MasterRekaman::where('id', $request->e_id)
                    ->update([
                        'rekaman_fullname' => $request->e_rekaman_fullname,
                        'rekaman_alamat' => $request->e_rekaman_alamat,
                        'rekaman_phone' => $request->e_rekaman_phone
                    ]);
        return redirect('/admin/data_rekaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterRekaman  $masterRekaman
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterRekaman $masterRekaman)
    {
        //
        Alert::success('Success', 'Data Terhapus');
        MasterRekaman::destroy($masterRekaman->id);
        // return redirect('/admin/data_rekaman');
    }
}
