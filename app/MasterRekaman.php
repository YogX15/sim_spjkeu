<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterRekaman extends Model
{
    //
    protected $table = 'master_rekaman';
    protected $fillable = ['rekaman_fullname','rekaman_alamat','rekaman_phone'];
}
