<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<!-- Site favicon -->
	<link rel="shortcut icon" href="{{asset ('login2/images/favicon.ico')}}">
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,600,700" rel="stylesheet">
	<!-- Icon Font -->
	<link rel="stylesheet" href="{{asset ('login2/fonts/ionicons/css/ionicons.css')}}">
	<!-- Text Font -->
	<link rel="stylesheet" href="{{asset ('login2/fonts/font.css')}}">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{asset ('login2/css/bootstrap.css')}}">
	<!-- Normal style CSS -->
	<link rel="stylesheet" href="{{asset ('login2/css/style.css')}}">
	<!-- Normal media CSS -->
	<link rel="stylesheet" href="{{asset ('login2/css/media.css')}}">
</head>
<body>
	<main class="cd-main">
		<section class="cd-section index visible ">
			<div class="cd-content style1">
				<div class="login-box d-md-flex align-items-center">
					<h1 class="title">Sim-SPJKEU</h1>
					<h3 class="subtitle">S.I Manajemen Surat Pertanggung Jawaban Keuangan</h3>
					<div class="login-form-box">
						<div class="login-form-slider">
							<!-- login slide start -->
							<div class="login-slide slide login-style1">
								<form method="POST" action="{{ route('login') }}">
                        			@csrf
									
									<div class="form-group">
										<label class="label">Email</label>
										<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
                                		@enderror
									</div>
									<div class="form-group">
										<label class="label">Password</label>
										<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

											@error('password')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
									</div>
									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="customCheck1">
											<label class="custom-control-label" for="customCheck1">Remember me</label>
										</div>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary">
											{{ __('Login') }}
										</button>

										@if (Route::has('password.request'))
											<a class="btn btn-link" href="{{ route('password.request') }}">
												{{ __('Forgot Your Password?') }}
											</a>
										@endif
									</div>
								</form>
								<div class="sign-up-txt">
									Don't have an account? <a href="javascript:;" class="sign-up-click">Sign Up</a>
								</div>
								<div class="forgot-txt">
									<a href="javascript:;" class="forgot-password-click">Forgot Password</a>
								</div>
							</div>
							<!-- login slide end -->
							<!-- signup slide start -->
							<div class="signup-slide slide login-style1">
								<div class="d-flex height-100-percentage">
									<div class="align-self-center width-90-percentage">
										<form>
											<div class="form-group">
												<label class="label">Name</label>
												<input type="text" class="form-control">
											</div>
											<div class="form-group">
												<label class="label">Email</label>
												<input type="email" class="form-control">
											</div>
											<div class="form-group">
												<label class="label">Password</label>
												<input type="password" class="form-control">
											</div>
											<div class="form-group">
												<label class="label">Confirm Password</label>
												<input type="password" class="form-control">
											</div>
											<div class="form-group padding-top-15px">
												<input type="submit" class="submit" value="Sign Up">
											</div>
										</form>
										<div class="sign-up-txt">
											if you have an account? <a href="javascript:;" class="login-click">login</a>
										</div>
									</div>
								</div>
							</div>
							<!-- signup slide end -->
							<!-- forgot password slide start -->
							<div class="forgot-password-slide slide login-style1">
								<div class="d-flex height-100-percentage">
									<div class="align-self-center width-100-percentage">
										<form>
											<div class="form-group">
												<label class="label">Enter your email address to reset your password</label>
												<input type="email" class="form-control">
											</div>
											<div class="form-group">
												<input type="submit" class="submit" value="Submit">
											</div>
										</form>
										<div class="sign-up-txt">
											if you remember your password? <a href="javascript:;" class="login-click">login</a>
										</div>
									</div>
								</div>
							</div>
							<!-- forgot password slide end -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<div id="cd-loading-bar" data-scale="1"></div>
	<!-- JS File -->
	<script src="{{asset ('login2/js/modernizr.js')}}"></script>
	<script type="{{asset ('login2/text/javascript" src="js/jquery.js')}}"></script>
	<script type="{{asset ('login2/text/javascript" src="js/popper.min.js')}}"></script>
	<script type="{{asset ('login2/text/javascript" src="js/bootstrap.js')}}"></script>
	<script src="{{asset ('login2/js/velocity.min.js')}}"></script>
	<script type="{{asset ('login2/text/javascript" src="js/script.js')}}"></script>
	@include('sweetalert::alert')
</body>
</html>