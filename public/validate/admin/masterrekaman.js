$(function () {
    $('#simpan').click(function() {
        $.validator.setDefaults({
            submitHandler: function (form) {
                Swal.fire({
                    title: 'Yakin ?',
                    text: "Ingin Simpan Data ?",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Simpan'
                }).then((result) => {
                    if (result.value) {
                        form.submit();
                    }
                })
            }
        });
        $('#quickForm').validate({
            rules: {
                rekaman_fullname: {
                    required: true,
                },
                rekaman_alamat: {
                    required: true,
                },
                rekaman_phone: {
                    required: true,
                },
            },
            messages: {
                rekaman_fullname: {
                    required: "Please insert fullname",
                },
                rekaman_alamat: {
                    required: "Please insert address",
                },
                rekaman_phone: {
                    required: "Please insert phone",
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    })

    $('#example1').on('click', '.view_data', function() {
        var id = $(this).data("id");
        $.ajax({
            url: "/admin/data_rekaman/"+id,
            type: "get",
            data: {"id": id},
            success: function(data) {
                $('#isi_modal').html(data);
                $('#modal-edit').modal('show');
            }
        });
    })

    $('#update').click(function() {
        $.validator.setDefaults({
            submitHandler: function (form) {
                Swal.fire({
                    title: 'Yakin ?',
                    text: "Ingin Update Data ?",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Update'
                }).then((result) => {
                    if (result.value) {
                        form.submit();
                    }
                })
            }
        });
        $('#quickForm_edit').validate({
            rules: {
                e_rekaman_fullname: {
                    required: true,
                },
                e_rekaman_alamat: {
                    required: true,
                },
                e_rekaman_phone: {
                    required: true,
                },
            },
            messages: {
                e_rekaman_fullname: {
                    required: "Please insert fullname",
                },
                e_rekaman_alamat: {
                    required: "Please insert address",
                },
                e_rekaman_phone: {
                    required: "Please insert phone",
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    })

    $('#example1').on('click', '.delete_data', function() {
        var id = $(this).data("id");
        var token = $(this).data("token");
        Swal.fire({
            title: 'Yakin ?',
            text: "Ingin Hapus Data ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "/admin/data_rekaman/"+id,
                    type: "delete",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token
                    },
                    success: function(data) {
                        location.reload();
                        return false;
                    }
                });
            }
        })
    })

})