<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/user/dashboard', 'HomeController@index')->name('home');
Route::get('/admin/dashboard', 'HomeController@admin')->name('admin.home')->middleware('level');

// halaman admin
Route::get('/admin/data_rekaman', 'Admin\MasterRekamanController@index');
Route::post('/admin/data_rekaman', 'Admin\MasterRekamanController@store');
Route::get('/admin/data_rekaman/{masterRekaman}', 'Admin\MasterRekamanController@edit');
Route::patch('/admin/data_rekaman', 'Admin\MasterRekamanController@update');
Route::delete('/admin/data_rekaman/{masterRekaman}', 'Admin\MasterRekamanController@destroy');